<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print">
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection">
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css">

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">

	<div id="header">
	
		<div id="logo">
		<TABLE BORDER="1" CELLPADDING="0" CELLSPACING="0" 
WIDTH="300" HEIGHT="200">

<TR>
<TD ALIGN="CENTER" ROWSPAN="3" WIDTH="100"><img src="images/images.jpg" width="200" alt="LOGO"></TD>
<TD ALIGN="CENTER" WIDTH="1000"><h6>Perangkat Lunak Beasiswa LPKIA</h6></TD>
</TR>
<TR>
<TD ALIGN="CENTER"></TD>
</TR>
<TR>
<TD ALIGN="CENTER">
Jln. Soekarno Hatta 456 Bandung<br>
Fax : 022786877 | Telepon : 022-768111</TD>
</TR>
</TABLE>
	</div><!-- header -->

	<div id="mainmbmenu">
	
		<?php 
		//uncomment MAIN MENU , RUBAH KE mbmenu
		/* $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'Home', 'url'=>array('/site/index')),
				//array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
				//array('label'=>'Contact', 'url'=>array('/site/contact')),
				// uncomment about dan contact , add menu kategori beasiswa 
				array('label'=>'Mahasiswa', 'url'=>array('/mahasiswa/admin'), 'visible'=>!Yii::app()->user->isGuest),
				array('label'=>'Beasiswa', 'url'=>array('/kategory/index')),
				array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); */
		 $this->widget('application.extensions.mbmenu.MbMenu',array(
            'items'=>array(
                array('label'=>'Home', 'url'=>array('/site/index')),
				array('label'=>'Mahasiswa', 'url'=>array('/mahasiswa/admin'), 'visible'=>!Yii::app()->user->isGuest),
				array('label'=>'Beasiswa', 'url'=>array('/kategory/index')),
                array('label'=>'Pengajuan', 'url'=>array('/pengajuan/index'),
                  'items'=>array(
                    array('label'=>'Daftar','url'=>'index.php?r=pengajuan/create'), 
                    array('label'=>'Daftar Antrian','url'=>'index.php?r=pengajuan/admin'),
                    array('label'=>'Hasil Seleksi','url'=>'index.php?r=pengajuan/index'),
                  ),
			      
                ),
                  array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
			 	  array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)),
    )); ?>
		
		</div><!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>

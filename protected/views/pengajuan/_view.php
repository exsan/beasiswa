<?php
/* @var $this PengajuanController */
/* @var $data Pengajuan */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Nama Mahasiswa')); ?>:</b>
	<?php echo CHtml::encode($data->idMahasiswa->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_antrian')); ?>:</b>
	<?php echo CHtml::encode($data->no_antrian); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ket')); ?>:</b>
	<?php echo CHtml::encode($data->ket); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Nama Beasiswa')); ?>:</b>
	<?php echo CHtml::encode($data->idBeasiswa->nama_beasiswa); ?>
	<br />


</div>
<?php
/* @var $this KategoryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Kategories',
);

//$this->menu=array(
//	array('label'=>'Create Kategory', 'url'=>array('create')),
//	array('label'=>'Manage Kategory', 'url'=>array('admin')),
//);
?>

<h1>Kategories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

<?php
/* @var $this KategoryController */
/* @var $model Kategory */

$this->breadcrumbs=array(
	'Kategories'=>array('index'),
	$model->id,
);

/*$this->menu=array(
	array('label'=>'List Kategory', 'url'=>array('index')),
	array('label'=>'Create Kategory', 'url'=>array('create')),
	array('label'=>'Update Kategory', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Kategory', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Kategory', 'url'=>array('admin')),
);
*/
?>

<h1>View Kategory #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nama_kategory',
	),
)); ?>

<?php
/* @var $this KategoryController */
/* @var $model Kategory */

$this->breadcrumbs=array(
	'Kategories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Kategory', 'url'=>array('index')),
	array('label'=>'Manage Kategory', 'url'=>array('admin')),
);
?>

<h1>Create Kategory</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
/* @var $this KategoryController */
/* @var $model Kategory */

$this->breadcrumbs=array(
	'Kategories'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Kategory', 'url'=>array('index')),
	array('label'=>'Create Kategory', 'url'=>array('create')),
	array('label'=>'View Kategory', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Kategory', 'url'=>array('admin')),
);
?>

<h1>Update Kategory <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
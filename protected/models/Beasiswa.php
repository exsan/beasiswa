<?php

/**
 * This is the model class for table "beasiswa".
 *
 * The followings are the available columns in table 'beasiswa':
 * @property integer $id
 * @property string $nama_beasiswa
 * @property integer $id_kategory
 *
 * The followings are the available model relations:
 * @property Kategory $idKategory
 * @property Pengajuan[] $pengajuans
 * @property Persyaratan[] $persyaratans
 */
class Beasiswa extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'beasiswa';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_beasiswa, id_kategory', 'required'),
			array('id_kategory', 'numerical', 'integerOnly'=>true),
			array('nama_beasiswa', 'length', 'max'=>35),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nama_beasiswa, id_kategory', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idKategory' => array(self::BELONGS_TO, 'Kategory', 'id_kategory'),
			'pengajuans' => array(self::HAS_MANY, 'Pengajuan', 'id_beasiswa'),
			'persyaratans' => array(self::HAS_MANY, 'Persyaratan', 'id_beasiswa'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama_beasiswa' => 'Nama Beasiswa',
			'id_kategory' => 'Id Kategory',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama_beasiswa',$this->nama_beasiswa,true);
		$criteria->compare('id_kategory',$this->id_kategory);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Beasiswa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

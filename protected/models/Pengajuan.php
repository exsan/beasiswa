<?php

/**
 * This is the model class for table "pengajuan".
 *
 * The followings are the available columns in table 'pengajuan':
 * @property integer $id
 * @property integer $id_mahasiswa
 * @property integer $no_antrian
 * @property string $ket
 * @property integer $id_beasiswa
 *
 * The followings are the available model relations:
 * @property Mahasiswa $idMahasiswa
 * @property Beasiswa $idBeasiswa
 */
class Pengajuan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pengajuan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_mahasiswa, no_antrian, ket, id_beasiswa', 'required'),
			array('id_mahasiswa, no_antrian, id_beasiswa', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_mahasiswa, no_antrian, ket, id_beasiswa', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idMahasiswa' => array(self::BELONGS_TO, 'Mahasiswa', 'id_mahasiswa'),
			'idBeasiswa' => array(self::BELONGS_TO, 'Beasiswa', 'id_beasiswa'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_mahasiswa' => 'Id Mahasiswa',
			'no_antrian' => 'No Antrian',
			'ket' => 'Ket',
			'id_beasiswa' => 'Id Beasiswa',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_mahasiswa',$this->id_mahasiswa);
		$criteria->compare('no_antrian',$this->no_antrian);
		$criteria->compare('ket',$this->ket,true);
		$criteria->compare('id_beasiswa',$this->id_beasiswa);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pengajuan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

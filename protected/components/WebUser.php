<?php
class WebUser extends CWebUser
{	
	private function loadModel()
	{
		$model = Akun::model()->findByPk(Yii::app()->akun->id);
		if($model == null)
			return false;
		return $model;
	}

	public function getLevel()
	{
		$model = $this->loadModel();
		if($model)
			return $model->idLevel;
		return 0;
	}
}
	?>